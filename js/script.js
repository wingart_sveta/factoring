$(function(){
	var resizeCols = function(){var i=$(window).height()-$('.page-header').outerHeight()-$('.page-footer').outerHeight();$('.page-body .crop').css('height',i);$('.scroller').niceScroll().resize().show();}
	if($('.page-body-scroll').length){$(window).on('load resize',function(e){resizeCols();});};
	$('.switcher .btn').click(function(){$(this).addClass('active').siblings().removeClass('active');return false;});
	if($('.selectpicker').length){$('.selectpicker').selectpicker();};
	$('.popup').click(function(event){
		var i=$(this).attr('href');
		$(i).arcticmodal();
		event.preventDefault();
	});
	$('.tabpanel .nav-tabs a').click(function (e){e.preventDefault();$(this).tab('show');});
	$('.drop-fix').on('click',function(event){event.stopPropagation();});
	$('.drop-close').on('click',function(event){$(this).parents().removeClass('open');});

	$(".tabs").each(function(){
		$(this).tabs()
	})
	

	var orderBtn = $(".header_order_btn"),
		headerOrder = $("#header_orders"),
		headerOrderListItem = $(".header_order_list_item"),
		toggleLink = $(".toggle_link");

	headerOrder.on("click", ".header_order_list_item", function(){
		var currentText1 = $(this).find("strong").text(),
			currentText2 = $(this).find("span").text();
		
		orderBtn.find("strong").text(currentText1);
		orderBtn.find("span").text(currentText2);
		headerOrder.removeClass("show_list");
	})

	orderBtn.on("click", function(){
    	headerOrder.toggleClass("show_list");
    })

    toggleLink.on("click", function(){
    	$(this).toggleClass("active");
    })


	/*--------*/
	$('.select_file').children('button').on('click',function(){$(this).prev().trigger('click')});
  
});